/*
 * main.c
 *
 * Created: 10/8/2011 2:15:14 PM
 * Author: Fahad Mirza
 */ 
#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "buffer.h"
#include "lcd_lib.h"
#include "uart0.h"
#include "uartgps.h"
#include "nmea.h"


const unsigned char  GPRSACTIVE[]	= "has been activated.";
const unsigned char	 NO_GPRS[]	= "+CME ERROR: 107\r\n";	// GPRS connection is not available
const unsigned char  RING[]		= "RING\r\n";				// Incoming call
const unsigned char  ATH[]		= "ATH\r";					// Hangup call
const unsigned char  CMTI[]		= "+CMTI:";					// New Message arrived
const unsigned char  CONFIG[]	= "Config";					// Initial string Change ApiKey and DeviceId
const unsigned char  RECHARGE[]	= "Recharge";				// Give order to activate P6 internet
const unsigned char  P6REPLY[]	= "type Y ";      



extern const unsigned char  *searchStrings[13];
unsigned char msgString[200]; // For storing GPRS data
unsigned short sendGpsSms=0;

unsigned int gprsSecCount = 0;

//GPS Port Interrupt Service Routine

ISR(USART1_RX_vect)
{
	gps_GetRxByte();
}



// GSM and GPRS Interrupt Service Routine

ISR(USART0_RX_vect)
{
	unsigned char data;                    //Local variable

	while ( !(UCSR0A & (1<<RXC0)) ) ;   
    data = UDR0;                           //Always read something

    rx_buffer[ rx_wr_i++ ] = data;         //Store new data
	
	

	////////////////////////// Check for overflow /////////////////////////////////
    if( rx_wr_i >= UART0_RX_BUFFER_SIZE )
    {
        rx_wr_i = 0;                       //Reset write index
        //rx_overflow = 1;                 //Set flag high
        rx_buffer[ rx_wr_i ] = '\0';
		//UCSR0B &= ~(1<<RXCIE0);          //Disable RX interrupt
    }
		
	
	/////////////////////// Check if there is no network /////////////////////////

    if ((NO_GPRS[NoGPRSIndex] == data) && (NoGPRSIndex <= 16))
    {
		NoGPRSIndex++;
    }
	else if(NoGPRSIndex <= 16)
	{
		NoGPRSIndex = 0;
	}
	
	
	////////////////////////// Check if there is RING ///////////////////////////

	if ((RING[RingIndex] == data) && (RingIndex <= 5))
    {
		RingIndex++;
		if (RingIndex > 5)
		{
			uart0_SentTxBytes(ATH);    // Hangup the call
			RingIndex = 0;	
			sendGpsSms = 1;
		}
    }
	else if(RingIndex <= 5)
	{
		RingIndex = 0;
	}
	
	
	///////////////////////// Check for new sms /////////////////////////////////
	
	if ((CMTI[CmtiIndex] == data) && (CmtiIndex <= 5))
    {
		CmtiIndex++;
    }
	else if(CmtiIndex <= 5)
	{
		CmtiIndex = 0;
	}
	////////////////////////////////////////////////////////////////////////////	
	
	
	if( searchFor[rx_i] == data )      //Test response match
    {
        rx_i++;

        if( !searchFor[rx_i] )         //End of string!
        {
            rx_i = 0;
            rx_ack = 1;                //Set new message flag
            UCSR0B &= ~(1<<RXCIE0);    // Disable RX interrupt
        }
    }
    else
    {
        rx_i = 0;                      //Not valid search pattern...start again.
    }
}


// Timer1 Interrupt Service Routine
ISR(TIMER1_COMPA_vect)
{
	gprsSecCount++;
	sec_count++;
	
	
	TIFR |= (1<<OCF1A);
}


int main(void)
{
	//unsigned char msgString[200]; // Now global, For storing GPRS data
	unsigned char *msg;           // For storing SMS
	int i,j;					  // General Purpose
	
    unsigned char ApiKey[8] = "Default";       // API_key String
	unsigned char DeviceID[20] = "mfjhskd567"; //"abcdefghij";//   // Device ID
	
	NoGpsData = 1;	// initialize the variable
	
	
	ACSR |= (1<<ACD); // Analog comparator disable in PORTE
	
	// PORT initialization/////////////////////////////////////////////
	DDRB |= (1<<DDB7); // RTS
	DDRB &= ~(1<<DDB6); // CTS
	DDRD |= (1<<DDD5) | (1<<DDD6); // GPS and OBD select
	DDRD &= ~(1<<DDD0); // INT0 pin for LCD
	DDRG |= (1<<DDG0) | (1<<DDG2); // GSMPWR, Relay
	
	PORTB &= ~(1<<PB7); // RTS
	PORTD &= ~(1<<PD6);  // OBD
	PORTD |= (1<<PD5);   // GPS
	PORTG &= ~(1<<PG0); // GSMPWR
	PORTG |= (1<<PG2);  // Relay
	
	
	/////////////////////////////////////////////////////////////////
	
	
	// Timer1 Initialization
	TCCR1B |= (1<<WGM12) | (1<<CS10) | (1<<CS12);  // CTC mode, Prescaler 1024
	OCR1AH = 0x3D;			// For 1s OCR value is 15625 (0x3D09)
	OCR1AL = 0x09;
	TCNT1H = 0;
	TCNT1L = 0;
	TIMSK |= (1<<OCIE1A);	// Enable CTC interrupt
	sei();
	////////////////////////////////////////////////////
	
	
	
	// Initialize the LCD
	LCDinit();
	LCDcursorOFF();
	LCDhome();
	LCDclr();
    Delay_s(1);
	LCDGotoString(1, 0, "Pi-Labs VTS");
	Delay_s(1);
	////////////////////////////////////
	
	
	
	/* This part was for knowing the time before proceed to other initilization
	while(1)
	{
		if (LF1_found == 1)
		{
			LF1_found = 0;
		
		if(nmeaProcess(&uart1RxBuffer) == NMEA_GPRMC)
		{
			break;
		}
		
		}		
	}*/
	
	
	
	// Initialize GPRS & GSM port USART0
	uart0_init();
	gprs_init();
	
	
	uart0_rx_reset();
	uart0_rx_on();
	////////////////////////////////////
	
	
	// Initialize GPS port USART1
	gps_uart1_init();
	gps_rx_on();
	gpsPWR_ON();
	uart0_rx_on();
	////////////////////////////////////
	
	
	LCDclr();
	Delay_s(1);
	LCDclr();
	_delay_ms(100);
	
	while(1)
    {
		
		///////////////////////////////////// Process GPS data /////////////////////////////////
		
		if(GPS_LF_found == 1)    // Found new line, start process data
		{
			GPS_LF_found = 0;
			if(nmeaProcess(&uart1RxBuffer)==NMEA_GPGGA)
			{
				LCDclr();
				
				_delay_ms(5);
				
				LCDclr();
			
				if(NoGpsData == 1)
				{
					LCDGotoString(0, 0, "No GPS Data");
				}
				else
				{
					LCDGotoString(0, 0, lat);
					LCDGotoString(1, 1, lon);
				}
			}
		}
		
		
		
		/////////////////////////////////////////////// Send data in website ///////////////////////////////////////////
		
		if ((gprsSecCount >= 30) && (GprsEmptyFlag == 0) && (NoGpsData == 0))
		{
			gps_rx_off();
			
			i = j = 0;
			while(DeviceID[i] !='\0')
			{
				msgString[j++] = DeviceID[i++];
			}
			msgString[j++] = ',';
			
			i=0;
			while(gpgga[i] !='\0')
			{
				msgString[j++] = gpgga[i++];
			}				
			msgString[j++] = ',';
			
			i=0;
			while(gprmc[i] !='\0')
			{
				msgString[j++] = gprmc[i++];
			}
			
			msgString[j++] = ',';
			
			msgString[j++] = '1';   // Engine status
			msgString[j] = '\0';
			
			
			
			sendGPRSdata(msgString);
			uart0_rx_reset();
			uart0_rx_on();
			NoGpsData = 1;
			gprsSecCount = 0;
			gps_rx_on();
		}
		
		///////////////////////////////////// Send GPS through sms ///////////////////////////////////////////
		
		if(sendGpsSms == 1)
		{
			sendGpsSms = 0;
			Delay_s(3);
			send_msg(msgString, "01833316060");
		}
		
		///////////////////////////////////// Parsing SMS ///////////////////////////////////////////
		
		/*if (CmtiIndex > 5)
		{
			CmtiIndex = 0;
			msg = read_msg();
			for (i=0; CONFIG[i] == msg[i];) // If 1st string is Config then start parsing
			{	
				i++;
				if(!CONFIG[i])  // Check if search for CONFIG is finish
				{
					i++;
					for (j=0; j<7; j++)
					{
						ApiKey[j] = msg[i++];
					}
					ApiKey[7] = '\0';
					for (j=0; msg[i] != '\0'; j++)
					{
						DeviceID[j] = msg[i++];
					}
					DeviceID[j] = '\0';
				}
			}
			
			if(GprsEmptyFlag == 1)
			{
				
				LCDclr();
				Delay_s(1);
				LCDGotoString(1, 0, msg);
				Delay_s(8);
				
				for (i=0; RECHARGE[i] == msg[i];) // Check if string is Recharge
				{
					i++;
					if(!RECHARGE[i])  // Check if search for Recharge is finish
					{
						LCDclr();
						Delay_s(1);
						LCDGotoString(1, 0, "Recharge found");
						Delay_s(5);
						
						send_msg("P1", "5000");
						GprsEmptyFlag = 0;
					}
				}
			}
			else if (GprsEmptyFlag == 2)
			{
				i = 0;
				while(msg[i] != '\0')
				{
					if(msg[i] == P6REPLY[0] && msg[i+1] == P6REPLY[1])
					{
						if (msg[i+2] == P6REPLY[2] && msg[i+3] == P6REPLY[3] && msg[i+4] == P6REPLY[4] && msg[i+5] == P6REPLY[5] && msg[i+6] == P6REPLY[6])
						{
							LCDGotoString(1, 0, "Sending Y");
							Delay_s(5);
							
							send_msg("Y","5000");
							GprsEmptyFlag = 3;
						}
						break;
					}
					else
						i++;
				}
			}
			else if(GprsEmptyFlag == 3)
			{
				for (i=0; GPRSACTIVE[i] == msg[i];) // Check if string is GPRSACTIVE
				{
					i++;
					if(!GPRSACTIVE[i])  // Check if search for GPRSACTIVE is finish
					{
						LCDGotoString(1, 0, "Net activated");
						Delay_s(5);
						
						GprsEmptyFlag = 0;
					}
				}
				i = 0;
				while(msg[i] != '\0')
				{
					if(msg[i] == GPRSACTIVE[0] && msg[i+1] == GPRSACTIVE)
					{
						if (msg[i+2] == GPRSACTIVE[2] && msg[i+3] == GPRSACTIVE[3] && msg[i+4] == GPRSACTIVE[4] && msg[i+5] == GPRSACTIVE[5] && msg[i+6] == GPRSACTIVE[6])
						{
							LCDGotoString(1, 0, "Net activated");
							Delay_s(5);
						
							GprsEmptyFlag = 0;
						}
						break;
					}
					else
						i++;
				}
			}				
									
			delete_msg();
		}*/
					
	}
		
					
}