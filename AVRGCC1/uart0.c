#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdlib.h>
#include "nmea.h"
#include "lcd_lib.h"
#include "uart0.h"
#include "uartgps.h"



/////////////////////////////////////////////////// Possible strings ////////////////////////////////////////////////

const unsigned char	 OK[]		= "OK\r\n";					// OK string
const unsigned char  CREG[]		= "+CREG: 0,1\r\n";			// Home network registered					
const unsigned char  READY[]	= "> ";						// Phone ready to receive text message
const unsigned char  CR_LF[]	= "\r\n";					// Carriage Return Line Feed
const unsigned char  IP_ST[]	= "STATE: IP STATUS\r\n";	// IP status. Ready for connection.
const unsigned char  CONNECT[]	= "CONNECT OK\r\n";			// Connect with web server complete
const unsigned char	 CLOSED[]	= "CLOSED";					// Connection closed
const unsigned char	 CLOSE2[]	= "CLOSE OK\r\n";					// Connection closed
const unsigned char	 IP_INI[]	= "IP INITIAL\r\n";			// IP at initial. Establish it from AT+CSTT
const unsigned char	 SHUT[]		= "SHUT OK\r\n";			// Forcefully Close all connection
const unsigned char	 PWR_DWN[]	= "NORMAL POWER DOWN\r\n";	// Power down the modem
const unsigned char	 CALL_RDY[]	= "Call Ready\r\n";			// Modem is ready
const unsigned char  GPRS_Header[]= "GET /server/index.php?r=DeviceData/SaveDeviceData&data=";// Send data
const unsigned char  GPRS_Footer[]= " HTTP/1.1\r\nHost: prohoree.com.bd\r\n";
const unsigned char  PhnNo[]	= "+8801833316060";			// Phone Number


const unsigned char  *searchStrings[13]  = {OK, READY, CR_LF, IP_ST, CONNECT, CLOSED, IP_INI, SHUT, PWR_DWN, CALL_RDY, CREG, CLOSE2}; // Initialize pointer


/////////////////////////////////////////// AT-Command set used ////////////////////////////////////////////////

//const unsigned char  ATH[]			= "ATH\r";							// Hangup call
const unsigned char  AT[]			= "AT\r";
const unsigned char  ATD[]			= "ATD*566*10#\r";						// Check GPRS account
const unsigned char  AT_CREG[]		= "AT+CREG?\r";							// Check Network
const unsigned char  AT_CGPSPWR[]	= "AT+CGPSPWR=1\r";						// Power On GPS
const unsigned char  AT_CGPSRST[]	= "AT+CGPSRST=0\r";						// RESET GPS in cold mode
const unsigned char  AT_CMGS[]		= "AT+CMGS=\"";							// Send message
const unsigned char  AT_CMGD[]		= "AT+CMGD=1\r";						// Delete message at index 1
const unsigned char  AT_CMGR[]		= "AT+CMGR=1\r";						// Read from index 1
const unsigned char  AT_CGATT[]     = "AT+CGATT=1\r";						// Attach GPRS Service




const unsigned char  AT_CSTT[]		= "AT+CSTT=\"INTERNET\",,\r";			// Start Task
const unsigned char  AT_CIICR[]		= "AT+CIICR\r";							// Connect GPRS
const unsigned char  AT_CIFSR[]		= "AT+CIFSR\r";							// Get Local IP Address
const unsigned char  AT_CIPSTATUS[]	= "AT+CIPSTATUS\r";						// Query Current Connection Status


const unsigned char  AT_CIPSTART[]	= "AT+CIPSTART=\"TCP\",\"184.107.237.202\",\"80\"\r"; // Connect with Pilabs
const unsigned char  AT_CIPSEND[]	= "AT+CIPSEND\r";						// Initialize sending
const unsigned char  AT_CIPCLOSE[]	= "AT+CIPCLOSE\r";						// Initialize sending
const unsigned char  AT_CIPSHUT[]	= "AT+CIPSHUT\r";						// Close all connection





//////////////////////////////////////////////// Debug variables ////////////////////////////////////////////////
unsigned int i=0, errorCount=0;
volatile int cipflag = 0;
volatile unsigned char CIPstring[40]=" ";
long int valid_date = 0;





/*  USART0 initialization
 *
 *  This function set correct baudrate and functionality of
 *  the USART.
 */

void uart0_init()
{
	
    #undef BAUD                // avoid compiler warning
    #define BAUD 9600
	#define F_CPU 16000000UL
	#include <util/setbaud.h>
	UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;
	
    UCSR0B |= ( 1 << RXEN0 ) | (1 << TXEN0);      //Enable receiver and transmitter
    UCSR0C |= ( 1 << UCSZ01 ) | ( 1 << UCSZ00 );  //8 data bit
	
	/////////////////////////////
	sec_count = 0;     // Timer variable. For counting seconds. 
	RingIndex = 0;     // Index variable for track "RING"
	NoGPRSIndex = 0;
	CmtiIndex = 0;
	GprsEmptyFlag = 0;
	/////////////////////////////
	
	uart0_rx_reset();   //Reset receive buffer after phone_init()
	uart0_rx_on();		// Enable Rx interrupt
	//sei();			// Global interrupt is enable at main()
}




/*  GPRS initialization
 *
 *  This function establish the GPRS connection
 */

void gprs_init()
{
	SetSearchString( OK_ );
	uart0_rx_on();			
	uart0_SentTxBytes(AT);
	if ( check_acknowledge(2) > 0)
	{
		
	}		
	else
	{
		LCDGotoString(0,1, "Init start..");
		Delay_s(1);
		SetSearchString( CALLRDY_ );
		uart0_rx_on();
		
		
		PORTG |= (1<<PG0);
		Delay_s(2);
		PORTG &= ~(1<<PG0);
		
		
		if ( check_acknowledge(35) > 0)
		{
			SetSearchString( CREG_ );
		
			LCDGotoString(0,1, "Check Netwrk..");
			Delay_s(1);
			
			check_network2:
			uart0_rx_on();
			uart0_SentTxBytes(AT_CREG);
			if ( check_acknowledge(5) > 0)
			{
				LCDclr();
				LCDGotoString(0,1, "CREG OK");
				Delay_s(1);
				//GetGprsValidDate();
			}
			else
			{
				LCDclr();
				LCDGotoString(0,1, "CREG Failed");
				goto check_network2;
			}
	
			configGPRS();
		}		
		else
		{
			LCDGotoString(0,1, "Init Problem");
			Delay_s(2);
		}
	}									
}



// Configuration of GPRS
void configGPRS()
{
	//unsigned char i=0;
	//long int cur_date = 0;
	
	Start:
	SetSearchString( OK_ );
	uart0_rx_on();
	uart0_SentTxBytes(AT_CGATT);
	
	if ( check_acknowledge(15) > 0)
	{
		LCDGotoString(0,1, "CGATT OK");
		
		uart0_SentTxBytes(AT_CSTT);
		uart0_rx_on();
		if ( check_acknowledge(4) > 0)
		{
			LCDGotoString(0,1, "CSTT OK   ");
				
			uart0_SentTxBytes(AT_CIICR);
			uart0_rx_on();
		
			if ( check_acknowledge(50) > 0)
			{
				LCDGotoString(0,1, "CIICR OK");
						
				uart0_SentTxBytes(AT_CIFSR);
				Delay_s(3);
					
				SetSearchString( IPST_ );
				uart0_rx_on();
				uart0_SentTxBytes(AT_CIPSTATUS);
				
				if ( check_acknowledge(15) > 0)
				{	
					LCDGotoString(0,1, "CIPSTATUS OK");
					Delay_s(1);
				}
			}							
		}
	}
	else if ( NoGPRSIndex>16 )   // CGATT Failed, +CME ERROR: 107, Check interrupt function
	{
		LCDGotoString(0,1, "No GPRS");
		Delay_s(2);
	
		NoGPRSIndex = 0;
		
		gps_rx_off();
	
		LCDGotoString(0,1, "Net error");
		Delay_s(2);
		/*
		cur_date = (((date[4]-'0')*10) + (date[5]-'0')) * 10000;		
		cur_date += (((date[2]-'0')*10) + (date[3]-'0')) * 100;
		cur_date += ((date[0]-'0')*10) + (date[1]-'0');*/
		
		/*if(cur_date >= valid_date)
		{	
			// GPRS credit is finish
		
			send_msg("vts a net nai.", PhnNo);
			GprsEmptyFlag = 1;
		}*/
		gps_rx_on();	
		uart0_rx_on();
	}
		
	else
	{
		goto Start;// temporarily net disabled
	}
}


void gpsPWR_ON()
{
	SetSearchString( OK_ );
	uart0_rx_on();
	uart0_SentTxBytes(AT_CGPSPWR);
	if ( check_acknowledge(5) > 0)
	{
		LCDGotoString(0,1, "CGPSPWR OK");
		Delay_s(1);
	
		uart0_SentTxBytes(AT_CGPSRST);
		uart0_rx_on();
	
		if ( check_acknowledge(5) > 0)
		{
			LCDGotoString(0,1, "CGPSRST OK");
			Delay_s(1);
		}
	}					
    
}



/*  Send data through GPRS
 *
 *  This function send Latitude and Longitude through GPRS. 
 */

void sendGPRSdata(unsigned char* str)
{
	
	uart0_rx_reset();
	SetSearchString( CONNECT_ );
	uart0_rx_on();
	uart0_SentTxBytes(AT_CIPSTART);
	
	if ( check_acknowledge(60) > 0)
	{
		LCDGotoString(0,1, "CIPSTART OK");
		Delay_s(1);
		
		SetSearchString( READY_ );
		uart0_rx_on();
		uart0_SentTxBytes(AT_CIPSEND);
		
		if ( check_acknowledge(35) > 0)
		{
			
			uart0_SentTxBytes(GPRS_Header);
			/*if (cipflag > 0)
			{
				cipflag = 0;
				uart0_SentTxBytes(CIPstring);
			}*/
			//else
			//{
			
			uart0_SentTxBytes(str);
			
				/*char str[10];
				itoa(count++,str,10);
				uart0_SentTxBytes(str);
						
				uart0_SentTxBytes("+");
				itoa(errorCount[0],str,10);
				uart0_SentTxBytes(str);
			}*/			
				
			uart0_SentTxBytes(GPRS_Footer);
			SetSearchString( OK_ );
			uart0_rx_on();
			uart0_putchar(10);
			uart0_putchar(10);
			uart0_putchar(26);	
					
			if ( check_acknowledge(5) > 0)
			{
				LCDGotoString(0,1,"DataSent OK");					
			}
			
			SetSearchString( CLOSE2_ );
			uart0_rx_on();
			uart0_SentTxBytes(AT_CIPCLOSE);
			
			if ( check_acknowledge(5) > 0)
			{
				LCDGotoString(0,1,"CIPCLOSE OK");
				Delay_s(1);
			}				
		}			
	}
	else
	{
		LCDGotoString(0,1,"Cipstart error");
		Delay_s(1);
		
		//cipflag = 1;
		//errorCount[0]++;
		/*LCDclr();
		uart0_rx_on();
		uart0_SentTxBytes(AT_CIPSTATUS);
		Delay_s(5);
		
		int j=0;
		uart0_rx_off();
		
		for(i=0; i<rx_wr_i; i++)   // As check acknowledge is failed, so receive buffer index is reset. CIPStatus data will store from the beginning of Rx Buffer
		{
			if((rx_buffer[i] != '\r') && (rx_buffer[i] != '\n') && (rx_buffer[i] != ' '))
			{
				CIPstring[j++] = rx_buffer[i];
			}
		}
		CIPstring[j] = '\0';*/
		
		checkError();
	}		
}



/* 
   Check GPRS function
   This function 1st check if the modem is hang. Then check net availability.
*/
void checkError( void )
{
	AT:
	LCDclr();
	uart0_rx_reset();
	SetSearchString( OK_ );
	uart0_rx_on();
	uart0_SentTxBytes(AT);
	if ( check_acknowledge(2) > 0)
	{	
		LCDGotoString(0,1, "AT OK");
		Delay_s(1);
		
		uart0_SentTxBytes(AT_CIPSHUT);   // reply check korar dorkar nei, karon por por 2bar cipshut dile error ashe
		Delay_s(2);
	
		LCDGotoString(0,1, "CIPSHUT OK");
		Delay_s(1);
		
		configGPRS();
			
	}															

	else                     // Modem hang korse. Restart the modem.
	{
		sec_count = 0;
		errorCount++;
		if (errorCount > 3)
		{
			errorCount = 0;
			
			LCDGotoString(0,1, "Initiating shutdown..");
			Delay_s(2);
			
			SetSearchString( PWRDWN_);
			uart0_rx_on();
			
			PORTB |= (1<<PB6);
			if ( check_acknowledge(12) > 0)
			{
				PORTB &= ~(1<<PB6);
				gprs_init();					
			}				
			
		}
		else
		{
			while(sec_count < 8);
			goto AT;
		}
	}		
}	


/* 
 * Check GPRS availability date
 */

void GetGprsValidDate()
{
	uart0_rx_reset();
	SetSearchString( OK_ );
	
	ATD_Level:
	uart0_rx_on();
	uart0_SentTxBytes(ATD);
	if ( check_acknowledge(15) > 0)
	{	
		for( i = rx_wr_i - 1; i >= 0; i--)    //Run through rx_buffer backwards
		{ 
			if (rx_buffer[i] == '.')
			{
				i--;
				break;
			}
		}
		valid_date = (((rx_buffer[i-1]-'0')*10) + (rx_buffer[i]-'0')) * 10000;		
		valid_date += (((rx_buffer[i-4]-'0')*10) + (rx_buffer[i-3]-'0')) * 100;
		valid_date += ((rx_buffer[i-7]-'0')*10) + (rx_buffer[i-6]-'0');
	}
	else
	{
		goto ATD_Level;
	}		
}




/*  
 *  RX interrupt disable + reset all variable
 *  
 */

void uart0_rx_reset( void )
{
    UCSR0B &= ~(1<<RXCIE0);        // Disable RX interrupt
    rx_i = rx_wr_i = 0;           //Init variables
    rx_overflow = rx_ack = 0;     //Zero overflow flag
    rx_buffer[ rx_wr_i ] = '\0';  //Buffer init.
}


/*  
 *  RX interrupt disable
 *  
 *  This function disable usart0's Rx interrupt  
 */
void uart0_rx_off( void )
{
	UCSR0B &= ~(1<<RXCIE0);        // Disable RX interrupt
}


/*  
 *	RX interrupt enable
 *
 *  This function enable usart0's Rx interrupt
 */

void uart0_rx_on( void )
{
    UCSR0B |= ( 1 << RXCIE0 );
}


/*  
 *	Set desired search string
 */

void SetSearchString( unsigned char Response )
{
    uart0_rx_off();				  // Disable RX interrupt
    searchFor = searchStrings[Response];  //Set desired search string
    searchStr = Response;         //Used in rx_isr
    rx_i = 0;
}




/*  Print const unsigned char string
 *
 *  parameter str  Pointer to the string
 */

void uart0_SentTxBytes( const unsigned char *str )
{
    for( ;*str != '\0'; )
    {
        uart0_putchar( *str++ );
    }
}




/* 
 *	Put char in transmit buffer
 */
void uart0_putchar( unsigned char data )
{

    unsigned int i;

    for( i = 0; !( UCSR0A & ( 1 << UDRE0 ) ); i++ ) // Wait for empty transmit buffer
    {
        if( i > TX_WAIT )                           //How long one should wait
        {
            break;                                  //Give feedback to function caller
        }
    }

    UDR0 = data;                                    // Start transmition 	
}




/*  Check acknowledge returned from phone
 *
 *  This function checks if an acknowledge
 *  has been received from the phone. A counting loop is also
 *  included to avoid waiting for a acknowledge that never arrives.
 *
 *	Return Value:
 *     1 Success, correct acknowledge
 *     0 Error, returned "ERROR" or timed out
 */

int check_acknowledge( unsigned char timeOut)
{
	sec_count = 0;
	for( ;( rx_ack == 0 ) && ( sec_count < timeOut ); )
	{
		char str[3];
		itoa(sec_count,str,10);
		LCDGotoString(14,1,str);
		_delay_ms(100);
	}						//Wait loop

	if( rx_ack > 0 )        //Everything worked out fine
    {
        rx_ack = 0;         //Reset flag
        return 1;
    }
    else                   //A timeout could result from no acknowledge, wrong acknowledge or buffer overrun
    {
        uart0_rx_reset();  //Reset buffer and interrupt routine
        return 0;          //Timed out, or wrong acknowledge from phone
    }
	
} 





/*  Send message
 *
 *  This function will take user defined message.
 *  If successful, the message will be forwarded to the connected GSM modem.
 *
 *	Return Value:
 *     1 Success, message sent
 *    -1 No "> " from phone
 *    -2 No message sent acknowledge
 */

int send_msg( unsigned char* str, unsigned char* no )
{
	SetSearchString( READY_ );              //Set READY to be search string
	uart0_SentTxBytes( AT_CMGS );           //Send msg
	uart0_SentTxBytes(no);
	uart0_SentTxBytes("\"\r"); 
    uart0_rx_on();                          //Receiver on
	
	if( check_acknowledge(6) > 0 )			// Get ">"
	{
		SetSearchString( OK_ );
				
		uart0_SentTxBytes(str);
		
		uart0_putchar( 26 );
		uart0_rx_on();

		if( check_acknowledge(10) > 0 )
		{
			return 1;
		}

		else
		{
			return -2;
		}
	}

	else
	{
		return -1;
	}
	

}







/*  Delay function
 *
 *  One second delay function. 
 *  Parameter takes the value of second.
 */

void Delay_s(char sec)
{
	char i,j;
	for(j=0; j<sec; j++)
	{
		for(i=0; i<5; i++)
		{
			_delay_ms(200);
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*  Read message from a given index
 *
 *  This function is used to read a newly arrived message
 *  from index 1. The message is decoded, and stored
 *  in the msgbuff.
 */

unsigned char* read_msg( void )
{
    unsigned char *msg_str;                      //Pointer to message
    //unsigned int i;

    msg_str = '\0';

    uart0_rx_reset();                            //Reset system
    SetSearchString( OK_ );                      //Set OK to be search string
    uart0_SentTxBytes( AT_CMGR );                //Read message
    uart0_rx_on();                               //Receiver on, wait for acknowledge

    if( check_acknowledge(7) > 0 )               //Read = OK
    {
        msg_str = get_msg();                     //Get message from the data returned from modem
		
		return msg_str;
    }
    else                                         //Read != OK
    {
        return '\0';
    }

}




/*  Get start of compressed string
 *
 *  When a new message has been read from a given index, thsi function\n
 *  will run through it, and find start of the user text.
 *
 */
unsigned char* get_msg( void )
{

    unsigned char *in_handle;
    int i, len;

    len       = str_trim();                                   //Trim off \r\nOK\r\n
    in_handle = str_gets();                                   //Get rx_buff

    in_handle += 5;                                           //Skip first \r\n

    for( i = 0; ( i < len ) && ( *in_handle++ != '\n' ); i++) //Loop until we get '\n'
    {
        ;
    }

    //Error
    if( i >= len )
    {
        in_handle = '\0';
    }

    return in_handle;
}



/*  Remove O, K, \r and \n
 *
 *  If the receive buffer have "OK" "\r" "\n"
 *  These characters will be deleted.
 *
 *	Return value:
 *   i   Length of trimmed buffer
 */

int str_trim( void )
{

    int i;
    unsigned char temp;

    for( i = rx_wr_i - 1; i >= 0; i--)                                                   //Run through rx_buffer backwards
    {
        temp = rx_buffer[i];                                                             //rx_buff[i] into temp
        if( ( temp != '\r' ) && ( temp != '\n' ) && ( temp != 'O' ) && ( temp != 'K' ) ) //If not equal to 'O', 'K', '\r' or '\n', break
        {
            break;                                                                       //Do break
        }
    }

    rx_buffer[ i+1 ] = '\0';                                                             //Terminate trimmed string

    return i;                                                                            //Return new length
}


/*  
 *	Return pointer to receive buffer
 */

unsigned char* str_gets( void )
{
  return rx_buffer;
}



/*  Delete a message from a given index
 *
 *  This function will use the "AT+CMGD" command to delete
 *  the message @ index 1
 *
 *	Return Value:
 *     1 Success
 *     0 Error
 */
int delete_msg( void )
{

    uart0_rx_reset();               //Reset system
    SetSearchString( OK_ );         //Set OK to be search string
    uart0_SentTxBytes( AT_CMGD );        //Delete message
    uart0_rx_on();                  //Receiver on

    if( check_acknowledge(2) > 0 )   //Delete = OK
    {
        return 1;
    }

    else                            //Delete != OK
    {
        return 0;
    }
}