#ifndef UARTGPS_H
#define UARTGPS_H
#include "buffer.h"


#define UART1_RX_BUFFER_SIZE 150

cBuffer uart1RxBuffer;
unsigned char uart1RxData[UART1_RX_BUFFER_SIZE];
volatile short GPS_LF_found;


void gps_uart1_init();
void gps_rx_off( void );
void gps_rx_on( void );
void gps_GetRxByte( void );


#endif