
#include <avr/io.h>
#include <avr/interrupt.h>
#include "uartgps.h"
#include "buffer.h"


/*  USART1 initialization
 *
 *  This function set correct baudrate and functionality of
 *  the USART.
 */


void gps_uart1_init()
{
    #undef BAUD // avoid compiler warning
    #define BAUD 4800
	#define F_CPU 16000000UL
	#include <util/setbaud.h>
	UBRR1H = UBRRH_VALUE;
    UBRR1L = UBRRL_VALUE;
	
    UCSR1B = ( 1 << RXEN1 );                     //Enable receiver
    UCSR1C = ( 1 << UCSZ11 ) | ( 1 << UCSZ10 );  //8 data bit
	
	bufferInit(&uart1RxBuffer, uart1RxData, UART1_RX_BUFFER_SIZE); // Initialize the USART1 receive buffer

	GPS_LF_found = 0;
    gps_rx_off();
}




/*  
 *  RX interrupt disable
 */
void gps_rx_off( void )
{
    UCSR1B &= ~(1<<RXCIE1);         // Disable RX interrupt
}



/*  
 *	RX interrupt enable
 */
void gps_rx_on( void )
{
    UCSR1B |= ( 1 << RXCIE1 );  // Enable RX interrupt
}


void gps_GetRxByte( void)
{
	unsigned char data;                             //Local variable

	while ( !(UCSR1A & (1<<RXC1)) ) ;   
    data = UDR1;                                    //Always read something
	
	if (data == '\n')
	{
		GPS_LF_found = 1;
	}
	if( !bufferAddToEnd(&uart1RxBuffer, data) )
	{
		// no space in buffer
		// count overflow
		gps_rx_off();
		bufferFlush(&uart1RxBuffer);
		gps_rx_on();
	}
}



