#ifndef UART0_H
#define UART0_H

#define TX_WAIT        65000                     // Timeout value
#define UART0_RX_BUFFER_SIZE 300			     // rx_buffer size
//#define UART0_RX_BUFFER_MASK  RX_BUFFER_SIZE - 2 // Used to set overflow flag

		

#define OK_         0                       // Used to look up in SetSearchString( Response )                      
#define READY_      1                       
#define CRLF_       2                       
#define IPST_       3
#define CONNECT_	4
#define CLOSED_		5
#define IPINI_		6
#define SHUT_		7
#define PWRDWN_		8
#define CALLRDY_	9
#define CREG_		10
#define CLOSE2_		11



// String's Index
volatile unsigned short NoGPRSIndex, RingIndex, CmtiIndex;

// Gprs Empty flag
volatile unsigned char GprsEmptyFlag;	

// Overflow and acknowledge flag
volatile int rx_overflow, rx_ack;

// private buffer
volatile unsigned char rx_buffer[UART0_RX_BUFFER_SIZE];

// Private pointer
volatile unsigned char searchStr;
volatile unsigned char  *searchFor;           // Flash pointer

// Buffer counter
volatile int rx_i;

// Buffer write index
volatile int rx_wr_i;


// Count the second
volatile unsigned char sec_count;





void uart0_init();
void uart0_rx_reset( void );
void uart0_rx_on( void );
void uart0_rx_off( void );
void SetSearchString( unsigned char Response );
void uart0_SentTxBytes( const unsigned char *str  );
void uart0_putchar( unsigned char data );
int check_acknowledge( unsigned char timeOut );
unsigned char* read_msg( void );
unsigned char* get_msg( void );
int str_trim( void );
unsigned char* str_gets( void );
int send_msg( unsigned char* str, unsigned char* no );
int delete_msg( void );
void gpsPWR_ON( void );
void gprs_init( void );
void configGPRS( void );
void GetGprsValidDate( void );
void sendGPRSdata(unsigned char* str);
void checkError( void );
void Delay_s(char sec);

#endif