#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "buffer.h"
#include "nmea.h"


void nmeaInit(void)
{
	
}



char* nmeaGetPacketBuffer(void)
{
	return NmeaPacket;
}

char nmeaProcess(cBuffer* rxBuffer)
{
	char foundpacket = NMEA_NODATA;
	char startFlag = FALSE;
	//u08 data;
	int i,j;

	// process the receive buffer
	// go through buffer looking for packets
	while(rxBuffer->datalength)
	{
		// look for a start of NMEA packet
		if(bufferGetAtIndex(rxBuffer,0) == '$')
		{
			// found start
			startFlag = TRUE;
			// when start is found, we leave it intact in the receive buffer
			// in case the full NMEA string is not completely received.  The
			// start will be detected in the next nmeaProcess iteration.

			// done looking for start
			break;
		}
		else
			bufferGetFromFront(rxBuffer);
	}
	
	// if we detected a start, look for end of packet
	if(startFlag)
	{
		for(i=1; i<(rxBuffer->datalength)-1; i++)
		{
			// check for end of NMEA packet <CR><LF>
			if((bufferGetAtIndex(rxBuffer,i) == '\r') && (bufferGetAtIndex(rxBuffer,i+1) == '\n'))
			{
				// have a packet end
				// dump initial '$'
				bufferGetFromFront(rxBuffer);
				// copy packet to NmeaPacket
				
				for(j=0; j<(i-1); j++)
				{
					// although NMEA strings should be 80 characters or less,
					// receive buffer errors can generate erroneous packets.
					// Protect against packet buffer overflow
					if(j<(NMEA_BUFFERSIZE-1))
						NmeaPacket[j] = bufferGetFromFront(rxBuffer);
					else
						bufferGetFromFront(rxBuffer);
				}
				// null terminate it
				NmeaPacket[j] = 0;
				// dump <CR><LF> from rxBuffer
				bufferGetFromFront(rxBuffer);
				bufferGetFromFront(rxBuffer);

				// found a packet
				// done with this processing session
				foundpacket = NMEA_UNKNOWN;
				break;
			}
		}
	}

	if(foundpacket)
	{
		// check message type and process appropriately
		if(!strncmp(NmeaPacket, "GPGGA", 5))
		{
			// process packet of this type
			nmeaProcessGPGGA(NmeaPacket);
			// report packet type
			foundpacket = NMEA_GPGGA;
		}
		else if(!strncmp(NmeaPacket, "GPRMC", 5))
		{
			// process packet of this type
			nmeaProcessGPRMC(NmeaPacket);
			// report packet type
			foundpacket = NMEA_GPRMC;
		}
	}
	else if(rxBuffer->datalength >= rxBuffer->size)
	{
		// if we found no packet, and the buffer is full
		// we're logjammed, flush entire buffer
		bufferFlush(rxBuffer);
	}
	return foundpacket;
}

void nmeaProcessGPGGA(char* packet)
{
	char i = 6, j = 0, k=0;                //i=6 because I like to parse the buffer after "$GPGGA" string
	NoGpsData = 0;
	
	
	while(packet[i] != ',')           // get UTC time [hhmmss.sss]
	{
		gpgga[j++] = packet[i++];				
	}
	gpgga[j++] = ',';			  
	i++;						  // next field: latitude		
	
	
	if (packet[i] == ',')		  //There is no GPS data.
	{
		i=i+2;
		NoGpsData = 1;           // Indicate the flag. 
	}
	else
	{
		while(packet[i] != ',')   // get latitude [ddmm.mmmmm]
		{
			lat[k++] = packet[i];
			gpgga[j++] = packet[i++]; 				
		}
		i++;
		
		lat[k] = '\0';
		gpgga[j++] = packet[i];     // GET: N/S indicator
		gpgga[j++] = ',';		
		i=i+2;
		k=0;
	}


    
	// get longitude [ddmm.mmmmm]
	while(packet[i] != ',')
	{
		lon[k++] = packet[i];
		gpgga[j++] = packet[i++];				
	}
    
	lon[k] = '\0';
	i++;
	// get: E/W indicator
	gpgga[j++] = packet[i];
	gpgga[j++] = ',';	
	i=i+2;	
	
	
	
	while(packet[i++] != ',');		// Skip: position fix status 		
	
	
	
	
	
	while(packet[i] != ',')			//Get: satellites used
	{
		gpgga[j++] = packet[i++];
	}
	gpgga[j++] = ',';				
	i++;							// next field: HDOP (horizontal dilution of precision)
	
	
	
	
	while(packet[i] != ',') 		// Get: HDOP
	{
		gpgga[j++] = packet[i++];
	}
	gpgga[j++] = ',';
	i++;							// next field: altitude

	
	
	
	while(packet[i] != ',')			// Get: altitude (in meters)
	{
		gpgga[j++] = packet[i++];				
	}
	gpgga[j] = '\0';	
	

	
	/*while(packet[i++] != ',');			// next field: altitude units, always 'M'
	while(packet[i++] != ',');			// next field: geoid seperation
	while(packet[i++] != ',');			// next field: seperation units
	while(packet[i++] != ',');			// next field: DGPS age
	while(packet[i++] != ',');			// next field: DGPS station ID*/
	while(packet[i++] != '*');			// skip(till): checksum
}	

void nmeaProcessGPRMC(char* packet)
{
	char i = 6, j = 0;
	

	while(packet[i++] != ',');		//skip: UTC time [hhmmss.sss]
	
	
	while(packet[i] != ',')			// Get: Status
	{
		gprmc[j++] = packet[i++];
	}
	gprmc[j++] = ',';
	i++;							// next field: Latitude
	
	
	while(packet[i++] != ',');		// Skip: Latitude
	while(packet[i++] != ',');		// Skip: N/S Indicator
	
	while(packet[i++] != ',');		// Skip: Longitude
	while(packet[i++] != ',');		// Skip: E/W Indicator
	
	while(packet[i] != ',')			// Get: Speed
	{
		gprmc[j++] = packet[i++];
	}	
	gprmc[j++] = ',';
	i++;							// next field: course
		
	
	while(packet[i] != ',')			// Get: Course
	{
		gprmc[j++] = packet[i++];
	}	
	gprmc[j++] = ',';
	i++;							// next field: Date
		
	
	while(packet[i] != ',')			// Get: Date
	{
		gprmc[j++] = packet[i++];
	}	
	gprmc[j] = '\0';

		
	/*while(packet[i++] != ',');			
	while(packet[i++] != ',');	*/
	while(packet[i++] != '*');		// Skip till: checksum
}