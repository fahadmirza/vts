#ifndef NMEA_H
#define NMEA_H

#include "buffer.h"

// constants/macros/typdefs
#define NMEA_BUFFERSIZE		80

// Message Codes
#define NMEA_NODATA		0	// No data. Packet not available, bad, or not decoded
#define NMEA_GPGGA		1	// Global Positioning System Fix Data
#define NMEA_GPVTG		2	// Course over ground and ground speed
#define NMEA_GPGLL		3	// Geographic position - latitude/longitude
#define NMEA_GPGSV		4	// GPS satellites in view
#define NMEA_GPGSA		5	// GPS DOP and active satellites
#define NMEA_GPRMC		6	// Recommended minimum specific GPS data
#define NMEA_UNKNOWN	0xFF// Packet received but not known

#define FALSE 0
#define TRUE  1

// variables
char NmeaPacket[NMEA_BUFFERSIZE];
volatile unsigned char lat[15], lon[15];

volatile unsigned char gpgga[50], gprmc[25];
//time[15], Lat[15], Lon[15], Alt[15], satUse[5], date[8], hdop[8], status[3], speed[10], course[10];
short NoGpsData;


// functions
void nmeaInit(void);
char* nmeaGetPacketBuffer(void);
char nmeaProcess(cBuffer* rxBuffer);
void nmeaProcessGPGGA(char* packet);
void nmeaProcessGPRMC(char* packet);

#endif